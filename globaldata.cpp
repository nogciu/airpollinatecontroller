#include "globaldata.h"

GlobalData::GlobalData()
{

}

GlobalData::~GlobalData()
{

}

bool GlobalData::getStop()
{
    boost::lock_guard<boost::mutex> lock(stopMutex_);
    return stop_;
}

void GlobalData::setStop(bool stop)
{
    boost::lock_guard<boost::mutex> lock(stopMutex_);
    stop_ = stop;
}

void GlobalData::initialize()
{
    std::cout << "Global data initialization " << std::endl;
}

GlobalData &GlobalData::getInstance()
{
    static GlobalData globalData_;
    return globalData_;
}

int GlobalData::getCurrentDirection()
{
    boost::lock_guard<boost::mutex> lock(currentSpeedMutex_);
    return currentDirection_;
}

void GlobalData::setCurrentDirection(int currentDirection)
{
    boost::lock_guard<boost::mutex> lock(currentSpeedMutex_);
    currentDirection_ = currentDirection;
}

float GlobalData::getCurrentSpeed()
{
    boost::lock_guard<boost::mutex> lock(currentSpeedMutex_);
    return currentSpeed_;
}

void GlobalData::setCurrentSpeed(float currentSpeed)
{
    boost::lock_guard<boost::mutex> lock(currentSpeedMutex_);
    currentSpeed_ = currentSpeed;
}

int GlobalData::getGoCommand()
{
    boost::lock_guard<boost::mutex> lock(goMutex_);
    return goCommand_;
}

void GlobalData::setGoCommand(int goCommand)
{
    boost::lock_guard<boost::mutex> lock(goMutex_);
    goCommand_ = goCommand;
}

bool GlobalData::getGo()
{
    boost::lock_guard<boost::mutex> lock(goMutex_);
    return go_;
}

void GlobalData::setGo(bool go)
{
    boost::lock_guard<boost::mutex> lock(goMutex_);
    go_ = go;
}

int GlobalData::getCurrentOperatingMode()
{
    boost::lock_guard<boost::mutex> lock(currentOperatingModeMutex_);
    return currentOperatingMode_;
}

void GlobalData::setCurrentOperatingMode(int currentOperatingMode)
{
    boost::lock_guard<boost::mutex> lock(currentOperatingModeMutex_);
    currentOperatingMode_ = currentOperatingMode;
}

bool GlobalData::getDeviceReady()
{
    boost::lock_guard<boost::mutex> lock(deviceReadyMutex_);
    return deviceReady_;
}

void GlobalData::setDeviceReady(bool deviceReady)
{
    boost::lock_guard<boost::mutex> lock(deviceReadyMutex_);
    deviceReady_ = deviceReady;
}

int GlobalData::getCurrentStatus()
{
    boost::lock_guard<boost::mutex> lock(currentStatusMutex_);
    return currentStatus_;
}

void GlobalData::setCurrentStatus(int currentStatus)
{
    boost::lock_guard<boost::mutex> lock(currentStatusMutex_);
    currentStatus_ = currentStatus;
}

int GlobalData::getCurrentError()
{
    boost::lock_guard<boost::mutex> lock(currentErrorMutex_);
    return currentError_;
}

void GlobalData::setCurrentError(int currentError)
{
    boost::lock_guard<boost::mutex> lock(currentErrorMutex_);
    currentError_ = currentError;
}
