#include "gpioreader.h"


GpioReader::GpioReader()
{

}

GpioReader::~GpioReader()
{

}

void GpioReader::initialize()
{
    LOGGER.addSystemMessage("Init GPIO reader");
    wiringPiSetup();
    relay1Pin_ = PARAMS.relay1Pin;
    relay2Pin_ = PARAMS.relay2Pin;
    relay3Pin_ = PARAMS.relay3Pin;
    relay4Pin_ = PARAMS.relay4Pin;

    pinMode(relay1Pin_, OUTPUT);
    pinMode(relay2Pin_ ,OUTPUT);
    pinMode(relay3Pin_, OUTPUT);
    pinMode(relay4Pin_, OUTPUT);
}

void GpioReader::setRelayState(short relayNumber, bool state)
{
    if(state == false)
    {
        switch(relayNumber)
        {
        case 1:
            digitalWrite(relay1Pin_, LOW);
            break;
        case 2:
            digitalWrite(relay2Pin_, LOW);
            break;
        case 3:
            digitalWrite(relay3Pin_, LOW);
            break;
        case 4:
            digitalWrite(relay4Pin_, LOW);
            break;
        default:
            break;
        }
    }
    else
    {
        switch(relayNumber)
        {
        case 1:
            digitalWrite(relay1Pin_, HIGH);
            break;
        case 2:
            digitalWrite(relay2Pin_, HIGH);
            break;
        case 3:
            digitalWrite(relay3Pin_, HIGH);
            break;
        case 4:
            digitalWrite(relay4Pin_, HIGH);
            break;
        default:
            break;
        }
    }

}
