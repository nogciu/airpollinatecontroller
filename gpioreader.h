#ifndef GPIOREADER_H
#define GPIOREADER_H

#include <stdio.h>
#include <wiringPi.h>
#include <iostream>
#include <boost/thread.hpp>
#include <math.h>

#include "params/params.h"
#include "Logger.h"



class GpioReader
{
public:
    GpioReader();
    ~GpioReader();

    void initialize();
    void setRelayState(short relayNumber, bool state);

private:
    short relay1Pin_;
    short relay2Pin_;
    short relay3Pin_;
    short relay4Pin_;
};

#endif // GPIOREADER_H
