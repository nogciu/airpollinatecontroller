#include "Logger.h"

Logger &Logger::getInstance()
{
    static Logger logger_;
    return logger_;
}


void Logger::initializate(std::__cxx11::string location)
{
    renameAndCompressLogs(location);
    std::string currentTime = QDateTime::currentDateTime().toString("dd.MM.yyyy_hh:mm:ss").toStdString() + ".log";
    std::string systemLogFilename;

    systemLogFilename = location + "systemLog.log";
    systemLog_.open(systemLogFilename.c_str(), std::ios_base::out);

    std::stringstream ss;
    ss << "Log from: " << currentTime.c_str() << std::endl << std::endl;

    systemLog_ << ss.str();
    systemLog_.flush();

    ss.str(std::string());

    ss << "CZAS ; ZDARZENIE" << std::endl;
    systemLog_ << ss.str();
    systemLog_.flush();
}

void Logger::addSystemMessage(std::string message)
{
    std::stringstream ss;
    ss << QDateTime::currentDateTime().toString("hh:mm:ss").toStdString().c_str() <<
          " ; " << message << std::endl;
    systemLogMutex_.lock();
    systemLog_ << ss.str();
    systemLog_.flush();
    systemLogMutex_.unlock();
}


Logger::Logger()
{

}

Logger::~Logger()
{
    systemLog_.close();
}


void Logger::renameAndCompressLogs(std::string systemLogPath)
{
    int fileCounter;
    boost::filesystem::path logsPath(systemLogPath);
    if (boost::filesystem::exists(logsPath))
    {
        fileCounter = std::count_if(
            boost::filesystem::directory_iterator(logsPath),
            boost::filesystem::directory_iterator(),
            static_cast<bool(*)(const boost::filesystem::path&)>(boost::filesystem::is_regular_file) );
    }
    else
    {
        std::cout << logsPath << " doesn't exists "<< std::endl;
        return;
    }

    std::string oldName = systemLogPath + "systemLog.log";
    std::string newName;
    if  (fileCounter < 9)
    {
        newName = systemLogPath + "systemLog_000" + std::to_string(fileCounter + 1) + ".log";
    }
    else if (fileCounter < 99)
    {
        newName = systemLogPath + "systemLog_00" + std::to_string(fileCounter + 1) + ".log";
    }
    else if (fileCounter < 999)
    {
        newName = systemLogPath + "systemLog_0" + std::to_string(fileCounter + 1) + ".log";
    }
    else
    {
        newName = systemLogPath + "systemLog_" + std::to_string(fileCounter + 1) + ".log";
    }

    std::rename(oldName.c_str(), newName.c_str());


    //compressing old files
    boost::filesystem::path oldLogsPath(systemLogPath + "old/");

    int logsCount = std::count_if(
        boost::filesystem::directory_iterator(logsPath),
        boost::filesystem::directory_iterator(),
        static_cast<bool(*)(const boost::filesystem::path&)>(boost::filesystem::is_regular_file) );

    int oldLogsCount = std::count_if(
        boost::filesystem::directory_iterator(oldLogsPath),
        boost::filesystem::directory_iterator(),
        static_cast<bool(*)(const boost::filesystem::path&)>(boost::filesystem::is_regular_file) );

    if (logsCount > MAX_FILE_NUMBER)
    {
        for (int i = 0; i < HOW_MANY_FILES_COMPRESS; i++)
        {
            std::string command;
            if(i < 9)
            {
                command = "mv " + systemLogPath + "systemLog_000" + std::to_string(i + 1) + ".log " + systemLogPath + "old/";
            }
            else if (i < 99)
            {
                command = "mv " + systemLogPath + "systemLog_00" + std::to_string(i + 1) + ".log " + systemLogPath + "old/";
            }
            else if (i < 999)
            {
                command = "mv " + systemLogPath + "systemLog_0" + std::to_string(i + 1) + ".log " + systemLogPath + "old/";
            }
            else
            {
                command = "mv " + systemLogPath + "systemLog_" + std::to_string(i + 1) + ".log " + systemLogPath + "old/";
            }
            system(command.c_str());
        }

        std::string command = "cd " + systemLogPath + "old/ && tar -czf oldLogs_" + std::to_string(oldLogsCount + 1) + ".tar.gz systemLog_* && rm systemLog_*";
        system(command.c_str());

        for (int i = HOW_MANY_FILES_COMPRESS; i < logsCount; i++)
        {
            std::string oldName;
            std::string newName;
            if(i < 9)
            {
                oldName = systemLogPath + "systemLog_000" + std::to_string(i + 1) + ".log";
                newName = systemLogPath + "systemLog_000" + std::to_string(i - HOW_MANY_FILES_COMPRESS + 1) + ".log";
            }
            else if (i < 99)
            {
                oldName = systemLogPath + "systemLog_00" + std::to_string(i + 1) + ".log";
                newName = systemLogPath + "systemLog_00" + std::to_string(i - HOW_MANY_FILES_COMPRESS + 1) + ".log";
            }
            else if (i < 999)
            {
                oldName = systemLogPath + "systemLog_0" + std::to_string(i + 1) + ".log";
                newName = systemLogPath + "systemLog_0" + std::to_string(i - HOW_MANY_FILES_COMPRESS + 1) + ".log";
            }
            else
            {
                oldName = systemLogPath + "systemLog_" + std::to_string(i + 1) + ".log";
                newName = systemLogPath + "systemLog_" + std::to_string(i - HOW_MANY_FILES_COMPRESS + 1) + ".log";

            }
            std::rename (oldName.c_str(), newName.c_str());
        }
    }
}
