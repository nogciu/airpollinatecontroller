#include "ads.h"

int i2cHandle;

static void beginTransmission(std::string i2cDevice, uint8_t i2cAddress)
{
    i2cHandle = open(i2cDevice.c_str(), O_RDWR);
    if(i2cHandle < 0)
    {
        fprintf(stderr, "Error while opening the i2c-1 device! Error: %s\n", strerror(errno));
    }

    if(ioctl(i2cHandle, I2C_SLAVE, i2cAddress) < 0)
    {
        fprintf(stderr, "Error while configuring the slave address %d. Error: %s\n", i2cAddress, strerror(errno));
    }
}


static void endTransmission(void)
{
    close(i2cHandle);
}


static void writeRegister(std::string i2cDevice, uint8_t i2cAddress, uint8_t reg, uint16_t value)
{
    beginTransmission(i2cDevice, i2cAddress);
    uint8_t lsb = (uint8_t)(value >> 8);
    uint8_t msb = (uint8_t)value;
    uint16_t payload = (msb << 8) | lsb;
    i2c_smbus_write_word_data(i2cHandle, reg, payload);
    endTransmission();
}

static uint16_t readRegister(std::string i2cDevice, uint8_t i2cAddress, uint8_t reg)
{
    beginTransmission(i2cDevice, i2cAddress);
    uint16_t res = i2c_smbus_read_word_data(i2cHandle, reg);
    endTransmission();
    uint8_t msb = (uint8_t)res;
    uint8_t lsb = (uint8_t)(res >> 8);
    return (msb << 8) | lsb;
}


ADS1015::ADS1015(std::string i2cDevice, uint8_t i2cAddress)
{
    LOGGER.addSystemMessage("ADS init");
    i2cDevice_ = i2cDevice;
    i2cAddress_ = i2cAddress;
    bitShift_ = 4;
    gain_ = GAIN_ONE;
    sps_  = SPS_128;
    setConversionDelay();
    initialize();
}


void ADS1015::setGain(adsGain_t gain)
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    gain_ = gain;
}

adsGain_t ADS1015::getGain()
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    return gain_;
}

void ADS1015::setSps(adsSps_t sps)
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    sps_ = sps;
    setConversionDelay();
}

adsSps_t ADS1015::getSps()
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    return sps_;
}

float ADS1015::getDistance(short channel)
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    return (getVoltage(channel) / 3.4) * 5.0  - 0.05;
}

float ADS1015::getVoltage(short channel)
{
    return (readADC_SingleEnded(channel) / 2047.0) * 4.096;
}

bool ADS1015::getIsInitialized()
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    return isInitialized_;
}

void ADS1015::initialize()
{
    int i2cHandle = open(i2cDevice_.c_str(), O_RDWR);
    if(i2cHandle < 0)
    {
        LOGGER.addSystemMessage("Error while opening the i2c-1 device!");
        isInitialized_ = false;
        fprintf(stderr, "Error while opening the i2c-1 device! Error: %s\n", strerror(errno));
        close(i2cHandle);
        return;
    }

    if(ioctl(i2cHandle, I2C_SLAVE, i2cAddress_) < 0)
    {
        LOGGER.addSystemMessage("Error while configuring the slave address");
        isInitialized_ = false;
        fprintf(stderr, "Error while configuring the slave address %d. Error: %s\n", i2cAddress_, strerror(errno));
        close(i2cHandle);
        return;
    }
    close(i2cHandle);

    LOGGER.addSystemMessage("ADS initialized");
    std::cout << "I2C initialized " << std::endl;
    isInitialized_ = true;
}

uint16_t ADS1015::readADC_SingleEnded(uint8_t channel)
{

    if (channel > 3)
    {
        return 0;
    }

    uint16_t config = ADS1015_REG_CONFIG_CQUE_NONE    | // Disable the comparator (default val)
                    ADS1015_REG_CONFIG_CLAT_NONLAT  | // Non-latching (default val)
                    ADS1015_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
                    ADS1015_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
                    ADS1015_REG_CONFIG_MODE_SINGLE;   // Single-shot mode (default)

    // Set PGA/voltage range
    config |= gain_;

    // Set the sample rate
    config |= sps_;

    // Set single-ended input channel
    switch (channel)
    {
    case (0):
      config |= ADS1015_REG_CONFIG_MUX_SINGLE_0;
      break;
    case (1):
      config |= ADS1015_REG_CONFIG_MUX_SINGLE_1;
      break;
    case (2):
      config |= ADS1015_REG_CONFIG_MUX_SINGLE_2;
      break;
    case (3):
      config |= ADS1015_REG_CONFIG_MUX_SINGLE_3;
      break;
    }

    // Set 'start single-conversion' bit
    config |= ADS1015_REG_CONFIG_OS_SINGLE;

    // Write config register to the ADC
    writeRegister(i2cDevice_, i2cAddress_, ADS1015_REG_POINTER_CONFIG, config);

    // Wait for the conversion to complete
    usleep(conversionDelay_);

    // Read the conversion results
    // Shift 12-bit results right 4 bits for the ADS1015
    return readRegister(i2cDevice_,i2cAddress_, ADS1015_REG_POINTER_CONVERT) >> bitShift_;
}


int16_t ADS1015::readADC_Differential_0_1()
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    // Start with default values
    uint16_t config = ADS1015_REG_CONFIG_CQUE_NONE    | // Disable the comparator (default val)
    ADS1015_REG_CONFIG_CLAT_NONLAT  | // Non-latching (default val)
    ADS1015_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
    ADS1015_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
    ADS1015_REG_CONFIG_MODE_SINGLE;   // Single-shot mode (default)

    // Set PGA/voltage range
    config |= gain_;

    // Set the sample rate
    config |= sps_;

    // Set channels
    config |= ADS1015_REG_CONFIG_MUX_DIFF_0_1;          // AIN0 = P, AIN1 = N

    // Set 'start single-conversion' bit
    config |= ADS1015_REG_CONFIG_OS_SINGLE;

    // Write config register to the ADC
    writeRegister(i2cDevice_, i2cAddress_, ADS1015_REG_POINTER_CONFIG, config);

    // Wait for the conversion to complete
    usleep(conversionDelay_);

    // Read the conversion results
    uint16_t res = readRegister(i2cDevice_, i2cAddress_, ADS1015_REG_POINTER_CONVERT) >> bitShift_;
    if (bitShift_ == 0)
    {
        return (int16_t)res;
    }
    else
    {
        // Shift 12-bit results right 4 bits for the ADS1015,
        // making sure we keep the sign bit intact
        if (res > 0x07FF)
        {
        // negative number - extend the sign to 16th bit
            res |= 0xF000;
        }
        return (int16_t)res;
    }
}

int16_t ADS1015::readADC_Differential_2_3()
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    // Start with default values
    uint16_t config = ADS1015_REG_CONFIG_CQUE_NONE    | // Disable the comparator (default val)
    ADS1015_REG_CONFIG_CLAT_NONLAT  | // Non-latching (default val)
    ADS1015_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
    ADS1015_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
    ADS1015_REG_CONFIG_MODE_SINGLE;   // Single-shot mode (default)

    // Set PGA/voltage range
    config |= gain_;

    // Set the sample rate
    config |= sps_;

    // Set channels
    config |= ADS1015_REG_CONFIG_MUX_DIFF_2_3;          // AIN2 = P, AIN3 = N

    // Set 'start single-conversion' bit
    config |= ADS1015_REG_CONFIG_OS_SINGLE;

    // Write config register to the ADC
    writeRegister(i2cDevice_, i2cAddress_, ADS1015_REG_POINTER_CONFIG, config);

    // Wait for the conversion to complete
    usleep(conversionDelay_);

    // Read the conversion results
    uint16_t res = readRegister(i2cDevice_, i2cAddress_, ADS1015_REG_POINTER_CONVERT) >> bitShift_;
    if (bitShift_ == 0)
    {
        return (int16_t)res;
    }
    else
    {
        // Shift 12-bit results right 4 bits for the ADS1015,
        // making sure we keep the sign bit intact
        if (res > 0x07FF)
        {
            // negative number - extend the sign to 16th bit
            res |= 0xF000;
        }
        return (int16_t)res;
    }
}

void ADS1015::startComparator_SingleEnded(uint8_t channel, int16_t threshold)
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    // Start with default values
    uint16_t config = ADS1015_REG_CONFIG_CQUE_1CONV   | // Comparator enabled and asserts on 1 match
    ADS1015_REG_CONFIG_CLAT_LATCH   | // Latching mode
    ADS1015_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
    ADS1015_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
    ADS1015_REG_CONFIG_MODE_CONTIN  | // Continuous conversion mode
    ADS1015_REG_CONFIG_MODE_CONTIN;   // Continuous conversion mode

    // Set PGA/voltage range
    config |= gain_;

    // Set the sample rate
    config |= sps_;

    // Set single-ended input channel
    switch (channel)
    {
        case (0):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_0;
            break;
        case (1):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_1;
            break;
        case (2):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_2;
            break;
        case (3):
            config |= ADS1015_REG_CONFIG_MUX_SINGLE_3;
            break;
    }

    // Set the high threshold register
    // Shift 12-bit results left 4 bits for the ADS1015
    writeRegister(i2cDevice_, i2cAddress_, ADS1015_REG_POINTER_HITHRESH, threshold << bitShift_);

    // Write config register to the ADC
    writeRegister(i2cDevice_, i2cAddress_, ADS1015_REG_POINTER_CONFIG, config);
}

int16_t ADS1015::getLastConversionResults()
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    // Wait for the conversion to complete
    usleep(conversionDelay_);

    // Read the conversion results
    uint16_t res = readRegister(i2cDevice_, i2cAddress_, ADS1015_REG_POINTER_CONVERT) >> bitShift_;
    if (bitShift_ == 0)
    {
        return (int16_t)res;
    }
    else
    {
        // Shift 12-bit results right 4 bits for the ADS1015,
        // making sure we keep the sign bit intact
        if (res > 0x07FF)
        {
            // negative number - extend the sign to 16th bit
            res |= 0xF000;
        }
        return (int16_t)res;
    }
}

void ADS1015::setConversionDelay()
{
    boost::lock_guard<boost::mutex> lock(mutex_);
    switch(sps_)
    {
      case SPS_128:
        conversionDelay_ = 1000000 / 128;
        break;
      case SPS_250:
        conversionDelay_ = 1000000 / 250;
        break;
      case SPS_490:
        conversionDelay_ = 1000000 / 490;
        break;
      case SPS_920:
        conversionDelay_ = 1000000 / 920;
        break;
      case SPS_1600:
        conversionDelay_ = 1000000 / 1600;
        break;
      case SPS_2400:
        conversionDelay_ = 1000000 / 2400;
        break;
      case SPS_3300:
        conversionDelay_ = 1000000 / 3300;
        break;
      case SPS_860:
        conversionDelay_ = 1000000 / 3300;
        break;
      default:
        conversionDelay_ = 8000;
        break;
    }
    conversionDelay_ += 100; // Add 100 us to be safe
}
