#include <iostream>
#include "globaldata.h"
#include "params/params.h"
#include "controller.h"
#include "Logger.h"

using namespace std;

int main(int argc, char *argv[])
{
    PARAMS.initializate();
    LOGGER.initializate();
    GLOBAL_DATA.initialize();
    Controller controller;
    controller.initializeComponents();
    controller.startMainLoop();
    return 0;
}
