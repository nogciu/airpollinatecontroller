#include "params.h"


Params::Params()
{

}

Params &Params::getInstance()
{
    static Params params;
    return params;
}

void Params::initializate(string filename)
{
    this->file_name = filename;
    load_params();
    std::cout << "Params initializaton from: " << file_name << std::endl;
}

void Params::load_params()
{
    cv::FileStorage fs;
    fs.open(this->file_name, cv::FileStorage::READ);

    serverPort = (int)fs["serverPort"];
    serverAdress = (string)fs["serverAdress"];
    deviceID = (int)fs["deviceID"];
    relay1Pin = (int)fs["relay1Pin"];
    relay2Pin = (int)fs["relay2Pin"];
    relay3Pin = (int)fs["relay3Pin"];
    relay4Pin = (int)fs["relay4Pin"];
    i2cBus = (string)fs["i2cBus"];
    i2cDeviceAdress = (int)fs["i2cDeviceAdress"];
    relayTimeOpen = (int)fs["relayTimeOpen"];

    fs.release();
}
