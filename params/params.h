#ifndef PARAMS_H
#define PARAMS_H

#include <string>
#include <iostream>
#include <opencv2/core/core.hpp>

#define PARAMS Params::getInstance()

using namespace std;

struct Params
{
public:
    static Params &getInstance();
    void initializate(std::string filename = "/opt/bdroix/params.yml");
    Params();
    std::string file_name;
    void load_params();

    int serverPort;
    string serverAdress;
    int deviceID;
    int relay1Pin;
    int relay2Pin;
    int relay3Pin;
    int relay4Pin;
    string i2cBus;
    int i2cDeviceAdress;
    int relayTimeOpen;
};

#endif // PARAMS_H
