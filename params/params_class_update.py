#! python
#-*- coding: utf-8 -*-
import os, sys
from shutil import copyfile

h = ""
cpp = ""

#początek plików
with open("params.h.txt") as h_template:
    for line in h_template:
        if line.find("[params]")>=0:
            break
        else:
            h += line
with open("params.cpp.txt") as cpp_template:
    for line in cpp_template:
        if line.find("[params]")>=0:
            break
        else:
            cpp += line

#parametry
with open('params.yml') as file:
    for line in file:
        if line.startswith('#') or line.startswith('%'):
            continue
        line = line.strip()
        if not line:
            continue

        l = line.split(':')
        name = l[0].strip()
        val  = l[1].strip()
        type = ""
        try:
            int(val)
            type = "int"
        except ValueError:
#            print "Not a int"
            try:
                float(val)
                type = "float"
            except ValueError:
#                print "Not a float"
                type = "string"

        h_line = "    " + type + " " + name + ";\n"
        h += h_line
        cpp_line = "    " + name + " = (" + type + ")fs[\"" + name + "\"];\n";
#        cpp_line = "    fs[\"" + name + "\"] >> " + name +";\n"
#        cpp_line += "cout << " + name + " << endl;\n"
        cpp += cpp_line


#koniec plików
with open("params.h.txt") as h_template:
    w = False
    for line in h_template:
        if w:
            h += line
        elif line.find("[params]")>=0:
            w = True
with open("params.cpp.txt") as cpp_template:
    w = False
    for line in cpp_template:
        if w:
            cpp += line
        elif line.find("[params]")>=0:
            w = True

#zapis plików
f_h   = open('params.h', 'w')
f_cpp = open('params.cpp', 'w')

f_h.write(h)
f_cpp.write(cpp)

f_h.close()
f_cpp.close()

#copy params.yml file
for dir in os.listdir(".."):
    if dir.startswith('build-bdroid-'):
        copyfile('params.yml', "../"+dir+"/params.yml")
