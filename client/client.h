#ifndef CLIENT_H
#define CLIENT_H

#include <cstdlib>
#include <iostream>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "../serverClientMessages/chat_message.hpp"
#include "globaldata.h"


using boost::asio::ip::tcp;

class ChatClient
{
public:
  ChatClient(boost::asio::io_service& io_service,
      tcp::resolver::iterator endpoint_iterator);

  void write(const ChatMessage& msg);
  void close();

  bool getConnected();


  bool getNewMessage();

  void setNewMessage(bool value);

  std::vector<std::string> getReceivedMessages();
  void clearReceivedMessages();


private:
  bool connected = false;
  bool newMessage = false;
  bool ping_ = false;

  tcp::resolver::iterator it;
  void do_connect(tcp::resolver::iterator endpoint_iterator);
  void do_read_header();
  void do_read_body();
  void do_write();

  std::vector < std::string > receivedMessages;

private:
    boost::asio::io_service& io_service_;
    tcp::socket socket_;
    ChatMessage readMessage_;
    chatMessageQueue writeMessages_;
    boost::mutex messageMutex_;
    boost::mutex connectedMutex_;
};



#endif // CLIENT_H
