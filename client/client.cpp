#include "client.h"

ChatClient::ChatClient(boost::asio::io_service& io_service,
                       tcp::resolver::iterator endpoint_iterator):
                       io_service_(io_service),
                       socket_(io_service)
{
    do_connect(endpoint_iterator);
}


void ChatClient::write(const ChatMessage &msg)
{
    io_service_.post(
    [this, msg]()
    {
        bool write_in_progress = !writeMessages_.empty();
        writeMessages_.push_back(msg);
        if (!write_in_progress)
        {
            do_write();
        }
    });
}

void ChatClient::close()
{
    io_service_.post([this]()
    {
        socket_.close();
    });
}

bool ChatClient::getConnected()
{
    boost::lock_guard<boost::mutex> lock(connectedMutex_);
    return connected;
}


bool ChatClient::getNewMessage()
{
    boost::lock_guard<boost::mutex> lock(messageMutex_);
    return newMessage;
}

void ChatClient::setNewMessage(bool value)
{
    boost::lock_guard<boost::mutex> lock(messageMutex_);
    newMessage = value;
}

void ChatClient::do_connect(tcp::resolver::iterator endpoint_iterator)
{
    it = endpoint_iterator;
    bool error = true;
    boost::asio::async_connect(socket_, endpoint_iterator,
    [&error, this](boost::system::error_code ec, tcp::resolver::iterator)
    {
        if (!ec)
        {
            this->connectedMutex_.lock();
            this->connected = true;
            this->connectedMutex_.unlock();
            do_read_header();
        }
        else
        {
//            std::cout << "EC " << ec.value() << std::endl;
            std::cout << "Trying to connect..." << std::endl;
            this->connectedMutex_.lock();
            this->connected = false;
            this->connectedMutex_.unlock();
            boost::this_thread::sleep(boost::posix_time::milliseconds(500));
            do_connect(this->it);
        }
    });
}

void ChatClient::do_read_header()
{
    boost::asio::async_read(socket_,
    boost::asio::buffer(readMessage_.data(), ChatMessage::headerLength),
    [this](boost::system::error_code ec, std::size_t /*length*/)
    {
        if (!ec && readMessage_.decode_header())
        {
            do_read_body();
        }
        else
        {
            std::cout << "SOCKET CLOSE - TRY TO RECONNECT" << std::endl;
            socket_.close();
            do_connect(it);
        }
    });
}

void ChatClient::do_read_body()
{
    boost::asio::async_read(socket_,
    boost::asio::buffer(readMessage_.body(), readMessage_.body_length()),
    [this](boost::system::error_code ec, std::size_t /*length*/)
    {
        if (!ec)
        {
            this->messageMutex_.lock();
            newMessage = true;
            std::string m(readMessage_.body(), readMessage_.body() + readMessage_.body_length());
            receivedMessages.push_back(m);
            this->messageMutex_.unlock();
//            std::cout.write(readMessage_.body(), readMessage_.body_length());
//            std::cout << "\n";
            do_read_header();
        }
        else
        {
            socket_.close();
            do_connect(this->it);
        }
    });
}

void ChatClient::do_write()
{
    boost::asio::async_write(socket_,
    boost::asio::buffer(writeMessages_.front().data(),
    writeMessages_.front().length()),
    [this](boost::system::error_code ec, std::size_t /*length*/)
    {
        if (!ec)
        {
            writeMessages_.pop_front();
            if (!writeMessages_.empty())
            {
                do_write();
            }
        }
        else
        {
            socket_.close();
            do_connect(this->it);
        }
    });
}

std::vector<std::string> ChatClient::getReceivedMessages()
{
    boost::lock_guard<boost::mutex> lock(messageMutex_);
    return receivedMessages;
}

void ChatClient::clearReceivedMessages()
{
    boost::lock_guard<boost::mutex> lock(messageMutex_);
    receivedMessages.clear();
}


