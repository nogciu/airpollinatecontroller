#ifndef CLIENTMANAGER_H
#define CLIENTMANAGER_H
#include <iostream>
#include <thread>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include "client.h"
#include "../serverClientMessages/messages.h"
#include "globaldata.h"
#include "Logger.h"
#include "ping.h"


class ClientManager
{
public:
    ClientManager();
    void startClient(std::string serverAdress, int serverPort, int clientID);
    bool writeMessageButtonPressed(MessageButtonPressed &m);
    bool writeMessageMessageDeviceStatus(MessageDeviceStatus &m);
    bool writeMessageStart(MessageStart &m);
    bool writeMessageVelDir(MessageVelocityDirection &m);
    bool writeMessageStop(MessageStop &m);
    bool writeMessage6(Message6 &m);
    bool writeMessage7(Message7 &m);
    bool clientConnected();
private:
    short myID_ = 0;
    int serverPort_ = 8001;
    std::string serverAdress_ = "192.168.0.1";
    ChatClient *client_;
    boost::asio::io_service ioService_;
    boost::mutex mutex_;

    void read();
    void pingServer();
    bool pinged_ = false;
    void sendMessage(const std::string &message);

};

#endif // CLIENTMANAGER_H
