#include "clientmanager.h"

ClientManager::ClientManager()
{

}

void ClientManager::startClient(std::__cxx11::string serverAdress, int serverPort, int clientID)
{
    serverAdress_ = serverAdress;
    serverPort_ = serverPort;
    myID_ = clientID;
    boost::thread ping(&ClientManager::pingServer, this);

    try
    {
        tcp::resolver resolver(ioService_);

        auto endpointIterator = resolver.resolve({serverAdress_, std::to_string(serverPort_)});

        client_ = new ChatClient(ioService_, endpointIterator);
        boost::thread clientThread(boost::bind(&boost::asio::io_service::run, boost::ref(ioService_)));
        boost::thread readThread(&ClientManager::read, this);
        LOGGER.addSystemMessage("CLient started");
    }
    catch (std::exception &e)
    {
        LOGGER.addSystemMessage("Start client exeption");
        std::string message ="Client nr "  + std::to_string(myID_) + " cought exeption: " + std::string(e.what());
        std::cout << message << std::endl;
        LOGGER.addSystemMessage(message);
    }

}

bool ClientManager::writeMessageButtonPressed(MessageButtonPressed &m)
{
    if(client_->getConnected() == true)
    {
        mutex_.lock();
        std::string message;
        m.messageFrom = myID_;
        message = messageButtonPressedToString(m);
        sendMessage(message);
//        message = "AA\nA";
        mutex_.unlock();
        return true;
    }
    else
        return false;

}

bool ClientManager::writeMessageMessageDeviceStatus(MessageDeviceStatus &m)
{
    if(client_->getConnected() == true)
    {
        mutex_.lock();
        std::string message;
        m.messageFrom = myID_;
        message = messageDeviceStatusToString(m);
        sendMessage(message);
        mutex_.unlock();
        return true;
    }
    else
        return false;
}

bool ClientManager::writeMessageStart(MessageStart &m)
{
    if(client_->getConnected() == true)
    {
        mutex_.lock();
        std::string message;
        m.messageFrom = myID_;
        message = messageStartToString(m);
        sendMessage(message);
        mutex_.unlock();
        return true;
    }
    else
        return false;
}

bool ClientManager::writeMessageVelDir(MessageVelocityDirection &m)
{
    if(client_->getConnected() == true)
    {
        mutex_.lock();
        std::string message;
        m.messageFrom = myID_;
        message = messageVelDirToString(m);
        sendMessage(message);
        mutex_.unlock();
        return true;
    }
    else
        return false;
}

bool ClientManager::writeMessageStop(MessageStop &m)
{
    if(client_->getConnected() == true)
    {
        mutex_.lock();
        std::string message;
        m.messageFrom = myID_;
        message = messageStopToString(m);
        sendMessage(message);
        mutex_.unlock();
        return true;
    }
    else
        return false;
}

void ClientManager::sendMessage(const std::__cxx11::string &message)
{
    ChatMessage msg;
    msg.body_length(message.length());
    std::memcpy(msg.body(), message.c_str(), msg.body_length());
    msg.encode_header();
    client_->write(msg);
}

void ClientManager::read()
{
    while (true)
    {
        if(client_->getConnected() == true)
        {
            if(client_->getNewMessage() == true)
            {
                client_->setNewMessage(false);
                std::vector < std::string > messages = client_->getReceivedMessages();

                client_->clearReceivedMessages();
                for (size_t i = 0; i < messages.size(); i++)
                {
                    int messageType = static_cast < int > (messages.at(i)[0]);
                    if(messageType == messageStart)
                    {
                        MessageStart m = stringToMessageStart(messages.at(i));
                        if(m.messageTo == myID_)
                        {
                            GLOBAL_DATA.setGo(true);
                            GLOBAL_DATA.setCurrentOperatingMode(m.operationMode);
                            GLOBAL_DATA.setGoCommand(m.direction);

                        }
                    }

                    else if(messageType == messageVelocityDirection)
                    {
                        MessageVelocityDirection m = stringToMessageVelDir(messages.at(i));
                        if(m.messageTo == myID_)
                        {
                            GLOBAL_DATA.setCurrentDirection(m.direction);
                            GLOBAL_DATA.setCurrentSpeed(m.velocity);

                        }

                    }
                    else if(messageType == messageStop)
                    {
                        MessageStop m = stringToMessageStop(messages.at(i));
                        if(m.messageTo == myID_ || m.messageTo == allDevices)
                        {
                            GLOBAL_DATA.setStop(true);
                        }
                    }
                }
            }
            else
            {
                boost::this_thread::sleep(boost::posix_time::milliseconds(1));
            }
        }
        else
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    }
}

bool ClientManager::clientConnected()
{
    bool pinged;
    mutex_.lock();
    pinged = pinged_;
    mutex_.unlock();
    return (client_->getConnected() && pinged);
}

void ClientManager::pingServer()
{
    bool pinged = false;
    while(true)
    {
        pinged = ping(serverAdress_);
        mutex_.lock();
        pinged_ = pinged;
        mutex_.unlock();
        if(pinged == true)
            usleep(500000);
    }

}
