TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += qt

LIBS += -lboost_system\
        -lboost_thread\
        -lboost_timer\
        -lboost_filesystem\
        -lwiringPi\
        -lrt\
        -lcrypt\
        `pkg-config opencv --libs`\


SOURCES += main.cpp \
    params/params.cpp \
    client/clientmanager.cpp \
    client/client.cpp \
    globaldata.cpp \
    ads.cpp \
    gpioreader.cpp \
    ../serverClientMessages/messages.cpp \
    controller.cpp \
    Logger.cpp \
    client/ping.cpp

HEADERS += \
    params/params.h \
    client/clientmanager.h \
    client/client.h \
    globaldata.h \
    ads.h \
    gpioreader.h \
    ../serverClientMessages/commonstructs.h \
    ../serverClientMessages/messages.h \
    ../serverClientMessages/chat_message.hpp \
    controller.h \
    Logger.h \
    client/ping.h
