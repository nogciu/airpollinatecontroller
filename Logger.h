#ifndef LOGGER_H
#define LOGGER_H

#include <fstream>
#include <QTime>
#include <boost/thread/mutex.hpp>
#include <boost/filesystem.hpp>
#include <QString>

#define MAX_FILE_NUMBER 2000
#define HOW_MANY_FILES_COMPRESS 1000
#define LOGGER Logger::getInstance()

class Logger
{
public:
    static Logger & getInstance();
    void initializate(std::string location = "/opt/bdroix/logs/");
    void addSystemMessage(std::string message);

private:
    Logger();
    ~Logger();
    Logger(Logger const &);
    void operator = (Logger const &);
    std::fstream systemLog_;
    boost::mutex systemLogMutex_;
    void renameAndCompressLogs(std::string path);
};

#endif // LOGGER_H
