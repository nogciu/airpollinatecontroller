#ifndef GLOBALDATA_H
#define GLOBALDATA_H

#include "../serverClientMessages/commonstructs.h"
#include "../serverClientMessages/messages.h"
#include <boost/thread.hpp>

#define GLOBAL_DATA GlobalData::getInstance()

class GlobalData
{
public:
    static GlobalData &getInstance();
    void initialize();

    int getCurrentError();
    void setCurrentError(int currentError);

    int getCurrentStatus();
    void setCurrentStatus(int currentStatus);

    bool getDeviceReady();
    void setDeviceReady(bool deviceReady);

    int getCurrentOperatingMode();
    void setCurrentOperatingMode(int currentOperatingMode);

    bool getGo();
    void setGo(bool go);

    int getGoCommand();
    void setGoCommand(int goCommand);

    float getCurrentSpeed();
    void setCurrentSpeed(float currentSpeed);

    int getCurrentDirection();
    void setCurrentDirection(int currentDirection);

    bool getStop();
    void setStop(bool stop);

private:
    GlobalData();
    ~GlobalData();
    void operator = (GlobalData const &);

    int currentError_ = unknownError;
    boost::mutex currentErrorMutex_;

    int currentStatus_ = starting;
    boost::mutex currentStatusMutex_;

    bool deviceReady_ = false;
    boost::mutex deviceReadyMutex_;

    int currentOperatingMode_;
    boost::mutex currentOperatingModeMutex_;

    bool go_ = false;
    int goCommand_ = Stop;
    boost::mutex goMutex_;

    float currentSpeed_;
    int currentDirection_;
    boost::mutex currentSpeedMutex_;

    bool stop_ = false;
    boost::mutex stopMutex_;
};

#endif // GLOBALDATA_H
