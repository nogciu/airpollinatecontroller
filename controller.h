#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "client/clientmanager.h"
#include "gpioreader.h"
#include "params/params.h"
#include "ads.h"
#include "globaldata.h"
#include "Logger.h"


class Controller
{
public:
    Controller();
    void initializeComponents();
    void startMainLoop();

private:
    ADS1015 *ads_;
    GpioReader *gpio_;
    ClientManager *client_;

    void sendMessages();

    void startPollinate();

    int openTime_;


};

#endif // CONTROLLER_H
