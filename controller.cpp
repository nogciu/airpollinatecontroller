#include "controller.h"

Controller::Controller()
{

}

void Controller::initializeComponents()
{
    LOGGER.addSystemMessage("Initialize components");
    openTime_ = PARAMS.relayTimeOpen;
    client_ = new ClientManager();
    client_->startClient(PARAMS.serverAdress, PARAMS.serverPort, PARAMS.deviceID);
    GLOBAL_DATA.setCurrentStatus(starting);
    GLOBAL_DATA.setDeviceReady(false);
    std::cout << "Waiting for connection..." << std::endl;
    LOGGER.addSystemMessage("Waiting for connection to main controller");
    while(client_->clientConnected() == false)
    {
        boost::this_thread::sleep(boost::posix_time::seconds(1));
    }
    LOGGER.addSystemMessage("Connected to main controller");
    boost::thread messages(&Controller::sendMessages, this);
    ads_ = new ADS1015(PARAMS.i2cBus, PARAMS.i2cDeviceAdress);

    gpio_ = new GpioReader();
    gpio_->initialize();

    while (true)
    {
        if(ads_->getIsInitialized() == false)
        {
            GLOBAL_DATA.setCurrentError(i2cError);
            GLOBAL_DATA.setCurrentStatus(error);
            LOGGER.addSystemMessage("I2C error");
            ads_->initialize();
        }
        if(client_->clientConnected() == false)
        {
            LOGGER.addSystemMessage("Connection to main controller lost");
        }

        if(ads_->getIsInitialized() == true &&
                client_->clientConnected() == true)
        {
            break;
        }
        boost::this_thread::sleep(boost::posix_time::seconds(1));
    }
    GLOBAL_DATA.setCurrentStatus(ready);
    GLOBAL_DATA.setCurrentError(noError);
    GLOBAL_DATA.setDeviceReady(true);
    LOGGER.addSystemMessage("System initialized");

}

void Controller::startMainLoop()
{
    LOGGER.addSystemMessage("Start main loop");
    bool errorLogged = false;
    while (true)
    {
        if(client_->clientConnected() == true && ads_->getIsInitialized() == true)
        {
            errorLogged = false;
            GLOBAL_DATA.setCurrentStatus(ready);
            GLOBAL_DATA.setDeviceReady(true);
            if(GLOBAL_DATA.getGo() == true)
            {
                LOGGER.addSystemMessage("Start signal");
                std::cout << "GO " << std::endl;
                if(client_->clientConnected() == true && GLOBAL_DATA.getDeviceReady() == true
                        && GLOBAL_DATA.getCurrentStatus() == ready)
                {
                    GLOBAL_DATA.setGo(false);
                    GLOBAL_DATA.setStop(false);
                    GLOBAL_DATA.setCurrentStatus(operating);
                    startPollinate();
                    GLOBAL_DATA.setCurrentStatus(ready);

                }

            }
//            std::cout << "MAIN " << std::endl;
        }
        else if(client_->clientConnected() == false)
        {
            GLOBAL_DATA.setCurrentStatus(unknownError);
            GLOBAL_DATA.setDeviceReady(false);
            if(errorLogged == false)
            {
                errorLogged = true;
                LOGGER.addSystemMessage("Connection to main controller lost");
            }
        }
        else if(ads_->getIsInitialized() == false)
        {
            if(errorLogged == false)
            {
                errorLogged = true;
                LOGGER.addSystemMessage("ADS error");
            }
            GLOBAL_DATA.setCurrentStatus(i2cError);
            GLOBAL_DATA.setDeviceReady(false);
            ads_->initialize();

        }
//        std::cout << "AA" << std::endl;
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
    }
}

void Controller::startPollinate()
{
    LOGGER.addSystemMessage("Pollination");
    std::cout << "POLLINATE " << std::endl;
    int breakTime = 50;
    float distance1 = 0.0;
    float distance2 = 0.0;

    while(true)
    {
        if(client_->clientConnected() == false || GLOBAL_DATA.getStop() == true)
        {
            LOGGER.addSystemMessage("End of pollination");
            if(client_->clientConnected() == false)
                LOGGER.addSystemMessage("Connection to main controller lost");
            if(GLOBAL_DATA.getStop() == true)
                LOGGER.addSystemMessage("Stop signal from main controller");
            std::cout << "BREAK " << std::endl;

            //zamkniecie wszystkich zaworow
            gpio_->setRelayState(1, false);
            gpio_->setRelayState(2, false);
            break;
        }
        //pobranie predkosci i przeliczenie na break time czas przerwy pomiedzy strzalami

        //pobranie odleglosci 1
        //distance1 = ads_->getDistance(0);
        //przeliczenie otwarcia zaworu 1

        //pobranie odleglosci 2
        //distance2 = ads_->getDistance(1);
        //przeliczenie otwarcia zaworu 2
        gpio_->setRelayState(1, true);
        gpio_->setRelayState(2, true);

        boost::this_thread::sleep(boost::posix_time::milliseconds(openTime_));

        gpio_->setRelayState(1, false);
        gpio_->setRelayState(2, false);

        boost::this_thread::sleep(boost::posix_time::milliseconds(breakTime));

    }
}

void Controller::sendMessages()
{
    MessageDeviceStatus message;
    while(true)
    {
        if(client_->clientConnected() == true)
        {
            message.messageTo = mainController;
            message.deviceReady = GLOBAL_DATA.getDeviceReady();
            message.currentError = GLOBAL_DATA.getCurrentError();
            message.currentStatus = GLOBAL_DATA.getCurrentStatus();
            client_->writeMessageMessageDeviceStatus(message);
        }
        else
        {
            GLOBAL_DATA.setCurrentError(unknownError);
            GLOBAL_DATA.setDeviceReady(false);
            GLOBAL_DATA.setCurrentStatus(error);
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
    }
}


